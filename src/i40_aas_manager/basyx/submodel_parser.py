from i40_aas_manager.basyx_registry_manager import BasyxRegistryManager


class SubmodelParser(BasyxRegistryManager):
    def __init__(self, config):
        self.config = config
        
    def parse_submodel(aas_id, submodel_id):
        endpoint = self.__getEndpoint__(aas_id=aas_id, submodel_id=submodel_id)
        sm = self.session.get(endpoint).json()
        