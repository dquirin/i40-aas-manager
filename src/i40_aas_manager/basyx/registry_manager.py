import urllib.parse
from ml4proflow_mods.basyx.connectors import BasyxRegistryConnector

class BasyxRegistryManager(BasyxRegistryConnector):
    def __init__(self, config):
        self.config = config
        
        self.config.setdefault("server_url", 'http://pyke.techfak.uni-bielefeld.de:15001')
        self.config.setdefault("base_iri", 'http://i40AutoServ.de/basyx/')
        self.config.setdefault("domain", ['cps', 'hw', 'vbs'])
        
        super().__init__(self.config['server_url'])
        
    def check_semantic_id(self, x: dict, semanticId: str) -> None:
        dummy = x['semanticId']['keys'][0]['value']
        assert dummy == semanticId, f'Is "{dummy}" but should be "{semanticId}"'
        
    def __assetID2aasID_converter__(self, asset_id):
        response = self.session.get(f'{self.url}/{urllib.parse.quote_plus(asset_id)}').json()
        return response['identification']["id"]
    
    def __getEndpoint__(self, aas_id, submodel_id=None):
        if submodel_id == None:
            response = self.session.get(f'{self.url}/{urllib.parse.quote_plus(aas_id)}').json()
        else:
            response = self.session.get(f'{self.url}/{urllib.parse.quote_plus(aas_id)}/submodels/{urllib.parse.quote_plus(submodel_id)}').json()
        return response['endpoints'][0]['address']

    def get_aas(self):
        response = self.get_registry()
        aas_list = {'Name':[], 'id': []}
        for i in response:
            for asset in self.config["domain"]:
                if f'{asset}' in i['identification']["id"]:
                    aas_list['Name'].append(i['asset']["idShort"])
                    aas_list['id'].append(i['identification']["id"])
        return aas_list 
    
    def get_submodels(self, aas_id):
        response = self.session.get(f'{self.url}/{urllib.parse.quote_plus(aas_id)}').json()
        submodels = {'Name': [], 'id': []}
        if aas_id == response['identification']["id"]:
            for i in response["submodels"]:
                submodels["Name"].append(i["idShort"])
                submodels['id'].append(i['identification']["id"])
        return {"Name": response["idShort"], "ID": response['identification']["id"], "Submodels": submodels}
    