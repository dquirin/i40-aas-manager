import urllib.parse
from ml4proflow_mods.basyx.connectors import BasyxAASShellConnector
from basyx.aas.adapter.json.json_serialization import AASToJsonEncoder

class BasyxServerManager(BasyxAASShellConnector):
    def __init__(self, config):
        self.config = config
        
        self.config.setdefault("server_url", 'http://pyke.techfak.uni-bielefeld.de:15000')
        self.config.setdefault("base_iri", 'http://i40AutoServ.de/basyx/')
        self.config.setdefault("domain", ['cps', 'hw', 'vbs'])
        
        super().__init__(self.config['server_url'])
        
    def put_aas(self, aas):
        aas_json_str = json.dumps(aas, cls=AASToJsonEncoder)
        aas_id = self._parse_identification(aas.identification.id)
        response = self.session.put(f'{self.url}/aasServer/shells/{aas_id}', data=aas_json_str)
        assert response.status_code == 200, f'PUT AAS failed with {response.text}'

    
    def put_submodels(self, aas_id, sm_list):
        for sm in sm_list:
            sm_json_str = json.dumps(sm, cls=AASToJsonEncoder, indent=2)
            response = self.session.put(f'{self.url}/aasServer/shells/{aas_id}/aas/submodels/{sm.id_short}',
                                        data = sm_json_str)
            assert response.status_code == 200, f'PUT SM failed with {response.text}'
        print(response.text)