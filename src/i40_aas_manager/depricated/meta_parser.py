import pkg_resources
from importlib import metadata

def get_metadata(package_name):
    meta = metadata.metadata(package_name)
    distribution = pkg_resources.get_distribution(package_name)
    install_reqs = distribution.requires()
    return meta, install_reqs

def generate_requirements_file(install_reqs):
    with open('requirements.txt', 'w') as f:
        for req in install_reqs:
            f.write(str(req) + '\n')