from basyx.aas import model
from basyx.aas.model import (AASReference, Reference, Key, KeyElements, KeyType
)

from i40_aas_manager.meta_parser import get_metadata, generate_requirements_file

def build_AINameplate(base_iri, package_name, repo_url, repo_branch, CI_job):
    meta, install_reqs = get_metadata(package_name.replace("-", "_"))
    generate_requirements_file(install_reqs)

    vbs_asset = model.Asset(kind=model.AssetKind.INSTANCE, 
                            identification=model.Identifier(f'{base_iri}type/{meta["Name"]}/asset', model.IdentifierType.IRI),
                            id_short=f'{meta["Name"].replace("-", "_")}',
                            description={"language": "en", "text":meta['Summary']},
            )
        
    contactInformation = model.SubmodelElementCollectionUnordered(id_short='ContactInformation',
                                                                  semantic_id = model.Identifier('https://admin-shell.io/zvei/nameplate/1/0/ContactInformations/ContactInformation',
                                                                                                 model.IdentifierType.IRI),
                                                                  value = [model.SubmodelElementCollectionUnordered(id_short="Email",
                                                                                                                    semantic_id = model.Identifier('0173-1#02-AAQ836#005', model.IdentifierType.IRI),
                                                                                                                    value = [model.MultiLanguageProperty(id_short="Email",
                                                                                                                                                         semantic_id = model.Identifier('0173-1#02-AAO198#002',
                                                                                                                                                                                        model.IdentifierType.IRI),
                                                                                                                                                         value={"EN": meta['Author-email']})]),
                                                                           model.MultiLanguageProperty(id_short="NameOfContact", semantic_id = model.Identifier('0173-1#02-AAO205#002', model.IdentifierType.IRI),
                                                                                                       value={"EN":meta['Author']}),
                                                                           model.Property(id_short="AdressOfAdditionalLink", semantic_id = model.Identifier('0173-1#02-AAQ326#002', model.IdentifierType.IRI),
                                                                                          value_type=model.datatypes.String, value=meta['Project-URL']), #meta['Home-page']
                                                                            ]
                                                                )
    details = model.SubmodelElementCollectionUnordered(id_short='Details',
                                                       value=[model.Property(id_short='FileExtension', value_type=model.datatypes.String, value='.py'),
                                                              model.Property(id_short='AIFramework', value_type=model.datatypes.String, value='???'),
                                                              model.Property(id_short='ProgramLanguage', value_type=model.datatypes.String, value='Python'),
                                                              model.File(id_short='Requirements', mime_type='txt', value=f'{repo_url.rstrip(".git")}/-/jobs/artifacts/{repo_branch}/raw/requirements.txt?job={CI_job}'),
                                                            ])
    
    ai_nameplate = model.Submodel(identification = model.Identifier(f'urn:{meta["Name"]}:AIModelNameplate', model.IdentifierType.IRI),
                                  id_short = 'AIModelNameplate',
                                  semantic_id=Reference((Key(type_=KeyElements.GLOBAL_REFERENCE,
                                                              local=False,
                                                              value='https://admin-shell.io/id/InterOpera/AIModelNameplate',
                                                              id_type=KeyType.IRI),)),
                                  submodel_element=[model.Property(id_short="URIOfTheProduct", value_type=model.datatypes.String, value=f'{base_iri}type/{meta["Name"]}/'),
                                                    model.MultiLanguageProperty(id_short="ModelName", value={"EN":meta['Name']}),
                                                    model.Property(id_short="Version", value_type=model.datatypes.String, value=meta['Version']),
                                                    contactInformation,
                                                    model.Property(id_short="Storage", value_type=model.datatypes.AnyURI, value=repo_url),
                                                    model.Property(id_short="Usage", value_type=model.datatypes.String, value=meta['Summary']),
                                                    details
                                                    ]
                                      )
    vbs_aas = model.AssetAdministrationShell(asset=vbs_asset,
                                             identification=model.Identifier(f'{base_iri}type{meta["Name"]}/aas/', model.IdentifierType.IRI),
                                             id_short=meta['Name'].replace("-", "_"),
                                             description={"language": "en", "text":f'{meta["Name"]} asset'},
                                             submodel = {AASReference.from_referable(ai_nameplate)},
                                             )
    return vbs_aas, ai_nameplate