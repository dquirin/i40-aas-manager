import sys
import argparse

from ml4proflow_mods.basyx.connectors import BasyxAASShellConnector
from i40_aas_manager.sw_builder import build_AINameplate


def main (arguments: list[str] = sys.argv[1:]) -> None:
    init_parser = argparse.ArgumentParser(description='Manage VBS AAS for i4.0 project', formatter_class=argparse.RawTextHelpFormatter, add_help=False)
    init_parser.add_argument('--vbs-name', default="My_VBS" , help="Identification of software assets")
    init_parser.add_argument('--repo-url', default='', help="URL of the repository")
    init_parser.add_argument('--repo-branch', default='master', help="Branch of the repository")
    init_parser.add_argument('--CI-job', default='deploy-aas', help="Job of CI pipeline")
    args, extra_args = init_parser.parse_known_args(arguments)

    parser = argparse.ArgumentParser(description='Manage AAS for i4.0 project', formatter_class=argparse.ArgumentDefaultsHelpFormatter, parents=[init_parser])
    parser.add_argument('--aas-url', default='http://pyke.techfak.uni-bielefeld.de:15000/', help='URL of the AAS server')
    parser.add_argument('--auth', default=None, help='not implemented yet')
    parser.add_argument('--iri-suffix', default='')



    args = parser.parse_args(arguments)
    dargs = vars(args)

    aas_server = BasyxAASShellConnector(args.aas_url)

    base_iri = f'http://i40AutoServ.de/basyx/{args.iri_suffix}vbs/'
    
    vbs_aas, ai_nameplate = build_AINameplate(base_iri, str(args.vbs_name), str(args.repo_url), str(args.repo_branch), str(args.CI_job))

    aas_server.put_aas(vbs_aas, [ai_nameplate])


if __name__ == '__main__':
    main(sys.argv[1:])
