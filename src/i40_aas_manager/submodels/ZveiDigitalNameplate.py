from i40_aas_manager.valueTypeDecoder import valueTypeDecoder

def parse_ZveiDigitalNameplate(self, sm):
    nameplate = {}
    for sm_element in sm['submodelElements']:
        nameplate[f'{sm_element["idShort"]}'] = valueTypeDecoder.CallMethodByName(sm_element["modelType"]["name"], sm_element)
    return nameplate