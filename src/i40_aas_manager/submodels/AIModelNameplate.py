from i40_aas_manager.valueTypeDecoder import valueTypeDecoder
from i40_aas_manager.semanticIDChecker import check_semantic_id
from i40_aas_manager.submodels.TemplateUpdater import update_template
from basyx.aas import model
from basyx.aas.model import (AASReference, Reference, Key, KeyElements, KeyType)


def template_AIModelNameplate():
    template = {'URIOfTheProduct': '', 'ModelName': '', 'Version': '', 'ContactInformation': 'REF to SM', 'Storage': '', 'Usage': '', 'KindOfLearning': '',
                        'Inputs': {'KindOfInput': '', 'Dimension':[{'Size': int(), 'Information': None}], "Preprocessing": None},
                        'Outputs': [{'Size': int(), 'Result': ''}],
                        'TrainingResults': [{}],
                        'Details': {'FileExtension': '', 'AIFramework': '', 'ProgramLanguage': '', 'Requirements': None},
                        'Dataset':{"Reference" : 'AnyIRI', "TimeStamp":None, 'SelectionCriteria': None},
                        'AIMethodSpecificInformation': {"OtherAI": [{}]}}
    return template

def build_AIModelNameplate(aas_id: str, ai_model_nameplate: dict = {}):
    template = template_AIModelNameplate()
    ai_model_nameplate=update_template(template, ai_model_nameplate)
    
    #Inputs
    #________________________________________________________________________________________________#
    smc_elements = []
    smc_elements.extend([model.Property(id_short='KindOfInput',
                                        semantic_id=model.Identifier('https://adminshell.io/id/InterOpera/AIModelNameplate/Inputs/KindOfInput',
                                                                     model.IdentifierType.IRI),
                                        value_type=model.datatypes.String,
                                        value=f'{ai_model_nameplate["Inputs"]["KindOfInput"]}'),
                        ])
    for i, dimension in enumerate(ai_model_nameplate['Inputs']['Dimension']):
        value = []
        value.extend([model.Property(id_short='Size',
                                     semantic_id=model.Identifier('https://adminshell.io/id/InterOpera/AIModelNameplate/Inputs/DimensionN/Size', model.IdentifierType.IRI),
                                     value_type=model.datatypes.String,
                                     value=f'{dimension["Size"]}')])
        value.extend([model.MultiLanguageProperty(id_short="Information",
                                                  semantic_id=model.Identifier('https://adminshell.io/id/InterOpera/AIModelNameplate/Inputs/DimensionN/Information', model.IdentifierType.IRI),
                                                    value={"EN":f'{dimension["Information"]}'}) for i in range(1) if dimension["Information"]])
        smc_elements.extend([model.SubmodelElementCollectionUnordered(id_short=f'Dimension{i}',
                                                                      semantic_id=model.Identifier('https://adminshell.io/id/InterOpera/AIModelNameplate/Inputs/DimensionN', model.IdentifierType.IRI),
                                                                      value = value)])
                                                                     
                             
    smc_elements.extend([model.File(id_short='Preprocessing',
                                    semantic_id=model.Identifier('', model.IdentifierType.IRI), 
                                    value=None) for i in range(1) if ai_model_nameplate['Inputs']["Preprocessing"]])
    
    inputs = model.SubmodelElementCollectionUnordered(id_short='Inputs',
                                                      semantic_id=model.Identifier('https://adminshell.io/id/InterOpera/AIModelNameplate/Inputs', model.IdentifierType.IRI),
                                                      value=smc_elements)
    
    #________________________________________________________________________________________________#
    #Outputs
    smc_elements = []
    for i, dimension in enumerate(ai_model_nameplate['Outputs']):
        value = []
        value.extend([model.Property(id_short='Size',
                                     semantic_id=model.Identifier('https://adminshell.io/id/InterOpera/AIModelNameplate/Outputs/DimensionN/Size', model.IdentifierType.IRI),
                                     value_type=model.datatypes.String,
                                     value=f'{dimension["Size"]}')])
        value.extend([model.MultiLanguageProperty(id_short="Result",
                                                  semantic_id=model.Identifier('https://adminshell.io/id/InterOpera/AIModelNameplate/Outputs/DimensionN/Result', model.IdentifierType.IRI),
                                                    value={"EN":f'{dimension["Result"]}'})])
        
        smc_elements.extend([model.SubmodelElementCollectionUnordered(id_short=f'Dimension{i}',
                                                                      semantic_id=model.Identifier('https://adminshell.io/id/InterOpera/AIModelNameplate/Outputs/DimensionN', model.IdentifierType.IRI),
                                                                      value = value)])
    
    outputs = model.SubmodelElementCollectionUnordered(id_short='Outputs',
                                                      semantic_id=model.Identifier('https://adminshell.io/id/InterOpera/AIModelNameplate/Outputs', model.IdentifierType.IRI),
                                                      value=smc_elements)
    #________________________________________________________________________________________________#
    #Training results
    smc_elements = []
    for result in ai_model_nameplate['TrainingResults']:
        for key,value in result:
            smc_elements.extend([model.Property(id_short=key,
                                                semantic_id=model.Identifier('https://adminshell.io/id/InterOpera/AIModelNameplate/TrainingResults/Result', model.IdentifierType.IRI),
                                                value_type=model.datatypes.String, #AnyXSDType,
                                                value=value)])
                                                
    
    training_results = model.SubmodelElementCollectionUnordered(id_short='TrainingResults',
                                                      semantic_id=model.Identifier('https://adminshell.io/id/InterOpera/AIModelNameplate/TrainingResults', model.IdentifierType.IRI),
                                                      value=smc_elements)
    #________________________________________________________________________________________________#
    #Plots 
    plots = None
    # ToDo
    
    #________________________________________________________________________________________________#
    #Details
    smc_elements = []
    smc_elements.extend([model.Property(id_short='FileExtension',
                                        semantic_id=model.Identifier('https://adminshell.io/id/InterOpera/AIModelNameplate/Details/DataEnding', model.IdentifierType.IRI),
                                        value_type=model.datatypes.String,
                                        value=f'{ai_model_nameplate["Details"]["FileExtension"]}'),
                         model.Property(id_short='AIFramework',
                                        semantic_id=model.Identifier('https://adminshell.io/id/InterOpera/AIModelNameplate/Details/AIFramework', model.IdentifierType.IRI),
                                        value_type=model.datatypes.String,
                                        value=f'{ai_model_nameplate["Details"]["AIFramework"]}'),
                         model.Property(id_short='ProgramLanguage',
                                        semantic_id=model.Identifier('https://adminshell.io/id/InterOpera/AIModelNameplate/Details/ProgramLanguage', model.IdentifierType.IRI),
                                        value_type=model.datatypes.String,
                                        value=f'{ai_model_nameplate["Details"]["ProgramLanguage"]}'),])
    smc_elements.extend([model.File(id_short='Requirements',
                                    semantic_id=model.Identifier('https://adminshell.io/id/InterOpera/AIModelNameplate/Details/Requirements', model.IdentifierType.IRI),
                                    mime_type='txt', #ToDo: Hard Coded Mime Type
                                    value=f'{ai_model_nameplate["Details"]["Requirements"]}') for i in range(1) if ai_model_nameplate["Details"]["Requirements"] # Working for Git Repos, but what is with real files?
                         ])
    details = model.SubmodelElementCollectionUnordered(id_short='Details',
                                                      semantic_id=model.Identifier('https://adminshell.io/id/InterOpera/AIModelNameplate/Details', model.IdentifierType.IRI),
                                                      value=smc_elements)
    #________________________________________________________________________________________________#
    #Dataset
    smc_elements = []
    smc_elements.extend([model.ReferenceElement(id_short='DatasetReference',
                                                semantic_id=model.Identifier('https://adminshell.io/id/InterOpera/AIModelNameplate/Dataset/DatasetReference', model.IdentifierType.IRI),
                                                value=None)]) #Reference((Key=type_=KeyElements.SUBMODEL, local=True, value='http://i40AutoServ/Datasetxxx))
    smc_elements.extend([model.Range(id_short='TimeStamp',
                                     semantic_id=model.Identifier('https://adminshell.io/id/InterOpera/AIModelNameplate/Dataset/TimeStamp', model.IdentifierType.IRI),
                                     value_type=model.datatypes.String, #AnyXSDType,
                                     #min=ai_model_nameplate["Dataset"]["TimeStamp"][0],
                                     #max=ai_model_nameplate["Dataset"]["TimeStamp"][1],
                                    ) for i in range(1) if ai_model_nameplate["Dataset"]["TimeStamp"]])
    smc_elements.extend([model.MultiLanguageProperty(id_short="SelectionCriteria",
                                                     semantic_id=model.Identifier('https://adminshell.io/id/InterOpera/AIModelNameplate/Outputs/DimensionN/Result', model.IdentifierType.IRI),
                                                     value={"EN":f'{ai_model_nameplate["Dataset"]["SelectionCriteria"]}'}) for i in range(1) if ai_model_nameplate["Dataset"]["SelectionCriteria"]])
    dataset = model.SubmodelElementCollectionUnordered(id_short='Dataset',
                                                      semantic_id=model.Identifier('https://adminshell.io/id/InterOpera/AIModelNameplate/Dataset', model.IdentifierType.IRI),
                                                      value=smc_elements)
    #________________________________________________________________________________________________#
    # AIMethodSpecificInformation 
    # ToDo: Currently only SMC Other AI is implemented
    smc_elements = []
    for info in ai_model_nameplate['AIMethodSpecificInformation']["OtherAI"]:
        for key,value in info:
            smc_elements.extend([model.Property(id_short=key,
                                                semantic_id=model.Identifier(f'https://adminshell.io/id/InterOpera/AIModelNameplate/AITypeSpecificInformation/OtherAI/{key}', model.IdentifierType.IRI),
                                                value_type=model.datatypes.String, #AnyXSDType,
                                                value=value)])
                                 
    other_ai = model.SubmodelElementCollectionUnordered(id_short='OtherAI',
                                                        semantic_id=model.Identifier('https://adminshell.io/id/InterOpera/AIModelNameplate/AITypeSpecificInformation/OtherAI', model.IdentifierType.IRI),
                                                        value=smc_elements)                             
    aimethodspecificinformation = model.SubmodelElementCollectionUnordered(id_short='AIMethodSpecificInformation',
                                                                           semantic_id=model.Identifier('https://adminshell.io/id/InterOpera/AIModelNameplate/AITypeSpecificInformation', model.IdentifierType.IRI),
                                                                           value=[other_ai])
    

    
    sm_elements = []
    sm_elements.extend([model.Property(id_short="URIOfTheProduct", 
                                        semantic_id=model.Identifier('0173-1#02-ABH173#001', model.IdentifierType.IRDI),
                                        value_type=model.datatypes.String, 
                                        value=f'{ai_model_nameplate["URIOfTheProduct"]}'),
                         model.MultiLanguageProperty(id_short="ModelName", 
                                                     semantic_id=model.Identifier('https://adminshell.io/id/InterOpera/AIModelNameplate/ModelName', model.IdentifierType.IRI),
                                                     value={"EN":f'{ai_model_nameplate["ModelName"]}'}),
                        model.Property(id_short="Version",
                                       semantic_id=model.Identifier('0173-1#02-AAS354#002', model.IdentifierType.IRDI),
                                       value_type=model.datatypes.String,
                                       value=f'{ai_model_nameplate["Version"]}'),
                        model.ReferenceElement(id_short="ContactInformation",
                                               semantic_id=model.Identifier('https://adminshell.io/id/InterOpera/AIDataset/Contactinformation', model.IdentifierType.IRI),
                                               value=None), # ToDo: Clarify, how to add a reference
                        model.Property(id_short="Storage",
                                       semantic_id=model.Identifier('https://adminshell.io/id/InterOpera/AIDataset/Contactinformation', model.IdentifierType.IRI),
                                       value_type=model.datatypes.String,
                                       value=f'{ai_model_nameplate["Storage"]}'),
                        model.MultiLanguageProperty(id_short="Usage",
                                                    semantic_id=model.Identifier('https://adminshell.io/id/InterOpera/AIModelNameplate/Usage', model.IdentifierType.IRI),
                                                    value={"EN":f'{ai_model_nameplate["Usage"]}'}),
                        model.MultiLanguageProperty(id_short="KindOfLearning",
                                                    semantic_id=model.Identifier('https://adminshell.io/id/InterOpera/AIModelNameplate/KindOfLearning', model.IdentifierType.IRI),
                                                    value={"EN":f'{ai_model_nameplate["KindOfLearning"]}'}),
                        inputs,
                        outputs,
                        training_results])
    
    sm_elements.extend([plots for i in range(1) if plots])
    sm_elements.extend([details])
    sm_elements.extend([dataset for i in range(1) if dataset])
    sm_elements.extend([aimethodspecificinformation])
    
    
    
    AIModelNameplate = model.Submodel(identification = model.Identifier(f'urn:{aas_id}:AIModelNameplate', model.IdentifierType.IRI),
                              id_short = 'AIModelNameplate',
                              semantic_id=Reference((Key(type_=KeyElements.GLOBAL_REFERENCE,
                                                          local=False,
                                                          value='https://admin-shell.io/id/InterOpera/AIModelNameplate',
                                                          id_type=KeyType.IRI),)),
                              submodel_element=sm_elements)
    return AIModelNameplate

if __name__ == '__main__':
    from basyx.aas.adapter.json.json_serialization import AASToJsonEncoder
    import json
    smc_aimodelnameplate = build_AIModelNameplate('TestAsset')
    print(json.dumps(smc_aimodelnameplate, cls=AASToJsonEncoder, indent = 2))

