from i40_aas_manager.valueTypeDecoder import valueTypeDecoder
from i40_aas_manager.semanticIDChecker import check_semantic_id
from i40_aas_manager.submodels.TemplateUpdater import update_template

from basyx.aas import model
from basyx.aas.model import (AASReference, Reference, Key, KeyElements, KeyType)
from datetime import date


def parse_SoftwareNameplate(sm):
    nameplate = {}
    for sm_element in sm['submodelElements']:
        nameplate[f'{sm_element["idShort"]}'] = valueTypeDecoder.CallMethodByName(sm_element["modelType"]["name"], sm_element)
    return nameplate

def template_SoftwareNameplate():
    template = {'UIROfTheProduct': 'TestURI', 'ManufacturerName': 'TestName', 'ManufacturerProductDesignation':'TestDesignation', 'Version': 'TestVersion', 'ReleaseDate': date.today() , 'BuildDate':date.today()}
    return template

def build_SoftwareNameplate(asset_kind: str, asset_id: str, sw_nameplate: dict = {}):
    # Check input
    assert asset_kind is not ['Instance', 'Type'], f'{asset_kind} is unknown as kind of asset'
    template = template_SoftwareNameplate()
    sw_nameplate=update_template(template, sw_nameplate)
    
        
    optional_input = {'ManufacturerProductDescription': None, 'ManufacturerProductFamily': None, 'ManufacturerProductType': None,
                      'SoftwareType': None, 'VersionName': None, 'VersionInfo': None, 'ReleaseNotes': None,'InstallationURI': None,
                      'InstallationFile': None, 'InstallerType': None, 'InstallationChecksum': None}
    
    for attr in optional_input.keys():
        if attr in sw_nameplate.keys():
            optional_input[attr] = sw_nameplate[attr]
    
    
    # Todo: 1: Input-Generation, 2: Actual just SWNameplate for Kind Type
    smc_elements = []
    if asset_kind == 'Instance':
        print("Not implemented yet")
    elif asset_kind == 'Type':
        smc_elements.extend([
            model.Property(id_short="UIROfTheProduct", semantic_id =model.Identifier('0173-1#02-AAY811#001', model.IdentifierType.IRDI), value_type = model.datatypes.String, value = f'{sw_nameplate["UIROfTheProduct"]}'),                 
            model.MultiLanguageProperty(id_short="ManufacturerName", semantic_id=model.Identifier('0173-1#02-AAO677#002', model.IdentifierType.IRDI), value={"EN":f'{sw_nameplate["ManufacturerName"]}'}),
            model.MultiLanguageProperty(id_short="ManufacturerProductDesignation", semantic_id=model.Identifier('0173-1#02-AAY811#002', model.IdentifierType.IRDI), value={"EN":f'{sw_nameplate["ManufacturerProductDesignation"]}'}),
                            ])
        smc_elements.extend([model.MultiLanguageProperty(id_short="ManufacturerProductDescription",
                                                          semantic_id=model.Identifier(
                                                                        'https://adminshell.io/idta/SoftwareNameplate/1/0/SoftwareNameplate/SoftwareNameplateType/ManufacturerProductDescription',
                                                                        model.IdentifierType.IRI),
                                                          value={"EN":f'{optional_input["ManufacturerProductDescription"]}'}) for i in range(1) if optional_input["ManufacturerProductDescription"]])
        smc_elements.extend([model.MultiLanguageProperty(id_short="ManufacturerProductFamily",
                                                          semantic_id= model.Identifier('0173-1#02-AAU731#001', model.IdentifierType.IRDI),
                                                          value={"EN":f'{optional_input["ManufacturerProductFamily"]}'}) for i in range(1) if optional_input["ManufacturerProductFamily"]])
        smc_elements.extend([model.MultiLanguageProperty(id_short="ManufacturerProductType",
                                                          semantic_id=model.Identifier('0173-1#02-AAO057#002', model.IdentifierType.IRDI),
                                                          value={"EN":f'{optional_input["ManufacturerProductType"]}'}) for i in range(1) if optional_input["ManufacturerProductType"]])
        smc_elements.extend([model.Property(id_short="SoftwareType",
                                             semantic_id=model.Identifier('https://adminshell.io/idta/SoftwareNameplate/1/0/SoftwareNameplate/SoftwareNameplateType/SoftwareType',
                                                                          model.IdentifierType.IRI),
                                             value_type = model.datatypes.String,
                                             value = f'{optional_input["SoftwareType"]}') for i in range(1) if optional_input["SoftwareType"]])
        smc_elements.extend([model.Property(id_short="Version",
                                             semantic_id =model.Identifier('https://adminshell.io/idta/SoftwareNameplate/1/0/SoftwareNameplate/SoftwareNameplateType/Version',
                                                                           model.IdentifierType.IRI),
                                             value_type = model.datatypes.String,
                                             value = f'{sw_nameplate["Version"]}')])
        smc_elements.extend([model.MultiLanguageProperty(id_short="VersionName",
                                                          semantic_id=model.Identifier('https://adminshell.io/idta/SoftwareNameplate/1/0/SoftwareNameplate/SoftwareNameplateType/VersionName',
                                                                                       model.IdentifierType.IRI),
                                                          value={"EN":f'{optional_input["VersionName"]}'}) for i in range(1) if optional_input["VersionName"]])
        smc_elements.extend([model.MultiLanguageProperty(id_short="VersionInfo",
                                                          semantic_id=model.Identifier('https://adminshell.io/idta/SoftwareNameplate/1/0/SoftwareNameplate/SoftwareNameplateType/VersionInfo',
                                                                                       model.IdentifierType.IRI),
                                                          value={"EN":f'{optional_input["VersionInfo"]}'}) for i in range(1) if optional_input["VersionInfo"]])
        smc_elements.extend([model.Property(id_short="ReleaseDate",
                                             semantic_id =model.Identifier('https://adminshell.io/idta/SoftwareNameplate/1/0/SoftwareNameplate/SoftwareNameplateType/ReleaseDate',
                                                                           model.IdentifierType.IRI),
                                             value_type = model.datatypes.Date,
                                             value = sw_nameplate["ReleaseDate"])])
        smc_elements.extend([model.MultiLanguageProperty(id_short="ReleaseNotes",
                                                          semantic_id=model.Identifier('https://adminshell.io/idta/SoftwareNameplate/1/0/SoftwareNameplate/SoftwareNameplateType/ReleaseNotes',
                                                                                       model.IdentifierType.IRI),
                                                          value={"EN":f'{optional_input["ReleaseNotes"]}'}) for i in range(1) if optional_input["ReleaseNotes"]])
        smc_elements.extend([model.Property(id_short="BuildDate",
                                             semantic_id =model.Identifier('https://adminshell.io/idta/SoftwareNameplate/1/0/SoftwareNameplate/SoftwareNameplateType/BuildDate',
                                                                           model.IdentifierType.IRI),
                                             value_type = model.datatypes.Date,
                                             value = sw_nameplate["BuildDate"])])
        smc_elements.extend([model.Property(id_short="InstallationURI",
                                             semantic_id=model.Identifier('https://adminshell.io/idta/SoftwareNameplate/1/0/SoftwareNameplate/SoftwareNameplateType/InstallationURI',
                                                                          model.IdentifierType.IRI),
                                             value_type = model.datatypes.String,
                                             value = f'{optional_input["InstallationURI"]}') for i in range(1) if optional_input["InstallationURI"]])
        # smc_elements.extend([model.Property(id_short="InstallationFile", # ToDo: Wait for Example AASX: Implementation unclear, BLob vs file
        #                                      semantic_id=model.Identifier('https://adminshell.io/idta/SoftwareNameplate/1/0/SoftwareNameplate/SoftwareNameplateType/InstallationFile',
        #                                                                   model.IdentifierType.IRI),
        #                                      value_type = model.Blob,
        #                                      value = [optional_input["InstallationFiles"]] for i in range(1) if optional_input["InstallationFiles"])])
        smc_elements.extend([model.Property(id_short="InstallerType",
                                             semantic_id=model.Identifier('https://adminshell.io/idta/SoftwareNameplate/1/0/SoftwareNameplate/SoftwareNameplateType/InstallerType',
                                                                          model.IdentifierType.IRI),
                                             value_type = model.datatypes.String,
                                             value = f'{optional_input["InstallerType"]}') for i in range(1) if optional_input["InstallerType"]])
        smc_elements.extend([model.Property(id_short="InstallationChecksum",
                                             semantic_id=model.Identifier('https://adminshell.io/idta/SoftwareNameplate/1/0/SoftwareNameplate/SoftwareNameplateType/InstallationChecksum',
                                                                          model.IdentifierType.IRI),
                                             value_type = model.datatypes.String,
                                             value = f'{optional_input["InstallationChecksum"]}') for i in range(1) if optional_input["InstallationChecksum"]])
        
        
        
        
        
        smc_softwarenameplate = model.SubmodelElementCollectionUnordered(id_short = "SoftwareNameplate_Type",
                                                                         semantic_id = model.Identifier('https://admin-shell.io/idta/SoftwareNameplate/1/0/SoftwareNameplateType',
                                                                                                        model.IdentifierType.IRI),
                                                                         value = smc_elements)

        smc_asset_kind = model.SubmodelElementCollectionUnordered(id_short = "SoftwareNameplate",
                                                                  semantic_id = model.Identifier('https://admin-shell.io/idta/SoftwareNameplate/1/0',
                                                                                                 model.IdentifierType.IRI),
                                                                  value = [smc_softwarenameplate])

        software_nameplate = model.Submodel(identification = model.Identifier(f'urn:{asset_id}:SoftwareNameplate', model.IdentifierType.IRI),
                                      id_short ="SoftwareNameplate",
                                      semantic_id=Reference((Key(type_=KeyElements.GLOBAL_REFERENCE,
                                                                  local=False,
                                                                  value='https://admin-shell.io/idta/SoftwareNameplate/1/0',
                                                                  id_type=KeyType.IRI),)),
                                      submodel_element=[smc_asset_kind])
        return software_nameplate
                                                        
                                                            
if __name__ == '__main__':
    from basyx.aas.adapter.json.json_serialization import AASToJsonEncoder
    import json
    
    smc_softwarenameplate = build_SoftwareNameplate('Type', 'TestAsset')
    print(json.dumps(smc_softwarenameplate, cls=AASToJsonEncoder, indent = 2)) 