def update_template(template, other_dict):
    for key, value in template.items():
        if isinstance(value, dict):
            # Wenn der Wert ein Dictionary ist, rufen Sie die Funktion rekursiv auf
            update_template(value, other_dict.get(key, {}))
        elif isinstance(value, list):
            # Wenn der Wert eine Liste ist, überprüfen Sie jedes Element
            for i, item in enumerate(value):
                if isinstance(item, dict):
                    update_template(item, other_dict.get(key, [{}])[i])
        else:
            # Andernfalls, wenn der Wert im anderen Dictionary vorhanden ist, aktualisieren Sie ihn
            if key in other_dict:
                template[key] = other_dict[key]
    return template