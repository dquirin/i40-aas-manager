class valueTypeDecoder():
    @staticmethod
    def CallMethodByName(method_name, *args, **kwargs):
        method = getattr(valueTypeDecoder, method_name, None)
        if method:
            return method(*args, **kwargs)
        else:
            raise AttributeError(f'Method {method_name} not found')
            
    @staticmethod
    def Property(sm_element):
        return sm_element['value']
    
    @staticmethod
    def MultiLanguageProperty(sm_element): 
        return f'{sm_element["value"][0]["language"]}:{sm_element["value"][0]["text"]}'
    
    @staticmethod
    def SubmodelElementCollection(sm_element):
        collection = {}
        for element in sm_element["value"]:
            collection[f'{element["idShort"]}'] = valueTypeDecoder.CallMethodByName(element["modelType"]["name"], element)
        return collection
    
    @staticmethod
    def File(sm_element):
        return sm_element['value']