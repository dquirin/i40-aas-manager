from setuptools import setup, find_namespace_packages

with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

setup(
    name="i40-aas-manager",
    version="0.0.1",
    author="Dennis Quirin",
    author_email="dquirin@techfak.uni-bielefeld.de",
    description="Generate HW and SW ass for i4.0 project",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="todo",
    project_urls={
        "Main framework": "test",
    },
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    entry_points={
        'console_scripts': ['sw-generator=i40_aas_manager.sw_cli:main',],
    },
    package_dir={"": "src"},
    packages=find_namespace_packages(where="src"),
    install_requires=["basyx-python-sdk", "requests", "ml4proflow-mods-basyx @ git+https://gitlab.ub.uni-bielefeld.de/ml4proflow/ml4proflow-basyx.git"],
    extras_require={
        "tests": ["pytest", 
          "pytest-html",
          "pytest-cov",
          "flake8",
          "mypy"],
        "docs": ["sphinx", "sphinx-rtd-theme", "recommonmark"],
    },
    python_requires=">=3.6", # todo
)
